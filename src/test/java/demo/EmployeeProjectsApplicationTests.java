package demo;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest
public class EmployeeProjectsApplicationTests {
	@Autowired
	private MockMvc mvc;
	@MockBean
	private ERepo e;
	@MockBean
	private Employee emp;

	@Test
	public void test() throws Exception {
		java.util.List<Employee> employees = Arrays.asList(new Employee(8315, "kella"), new Employee(8316, "kella"));
		when(e.findAll()).thenReturn(employees);


		verify(e, times(1)).findAll();
		verifyNoMoreInteractions(e);

	}

}
