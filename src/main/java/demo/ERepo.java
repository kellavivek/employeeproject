package demo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ERepo extends JpaRepository<Employee, Integer> {

}
